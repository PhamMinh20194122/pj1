const passport = require("passport");
const initPassportLocal = require("../app/controllers/auth/passportLocal");

const express = require("express");
const router = express.Router();
initPassportLocal();

const loginController = require("../app/controllers/loginController");
router.get("/", loginController.login);
router.post(
  "/",
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
  })
);

module.exports = router;
