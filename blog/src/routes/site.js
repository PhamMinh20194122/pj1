const express = require("express");
const router = express.Router();

const siteController = require("../app/controllers/SiteController");

router.get("/search", siteController.search);
router.get(
  "/",
  //   function isLoggedIn(req, res, next) {
  //     if (req.isAuthenticated()) return next();
  //     res.redirect("/login");
  //   },
  siteController.index
);

module.exports = router;
