const meRouter = require("./me");
const coursesRouter = require("./courses");
const siteRouter = require("./site");
const loginRouter = require("./login");

function route(app) {
  app.use("/login", loginRouter);
  app.use("/courses", coursesRouter);
  app.use("/me", meRouter);
  app.use("/", siteRouter);
}

module.exports = route;
