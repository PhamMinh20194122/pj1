const passport = require("passport");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const passportLocal = require("passport-local");
const UserModel = require("../../models/Login");

let LocalStrategy = passportLocal.Strategy;

let initPassportLocal = () => {
  passport.use(
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true,
      },
      async (req, email, password, done) => {
        try {
          if (email === "admin" && password === "admin") {
            let user = {
              email: email,
              password: password,
            };
            return done(null, user);
          } else {
            return done(null, false);
          }
          // let user = await UserModel.findOne({ email });
          // if (!user) {
          //   return done(null, false);
          // }

          // let checkPassword = await bcrypt.compare(password, user.password);

          // if (!checkPassword) {
          //   return done(null, false);
          // }
          // return done(null, user);
        } catch (error) {
          console.log(error);
          return done(null, user);
        }
      }
    )
  );
};

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

module.exports = initPassportLocal;
